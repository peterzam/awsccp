# AWS Architecting & Ecosystem

## Well Architected Framework General Guideline
- Stop gussing capacity
- Test system at production
- Automate to make easier
- Allow for evolutionary architectures
- Drive architectures using data
- Improve through game days (Simulate for the worst)

---

## Best Practices
- Scalability
- Disposable Resources
- Automation
- Loose Coupling (try microservices)
- Think in Services not in Servers

---

## 5 Pillars
- Operational Excellence
- Security
- Reliability
- Performance Efficiency
- Cost Optimization

---

## First : Operational Excellence
- Design Principles
    - Perform operations as code
    - Annotate documentation
    - Make frequent, small, reversible changes
    - Refine operations procedures frequently
    - Anticipate failure
    - Learn from all operational failures

- Services
    - Prepare (CloudFormation, Config)
    - Operate (CloudFormation, Config, CloudTrail, CloudWatch, X-Ray)
    - Evolve (CloudFormation, All CI/CD tools)

---

## Second : Security
- Design Principles
    - Implement a strong identity foundation
    - Enable traceability
    - Apply security at all layers
    - Automate security bes practices
    - Protect data in transit and at rest
    - Keep people away from data
   - Prepare for security events
- Services
    - Identity & Asscess Managemnet  (IAM, AWS-STS, MFA, Organizations)
    - Detective Controls (Config, CloudTrail, CloudWatch)
    - Infra Protection  (CloudFront, VPC, Shield, WAF, Inspector)
    - Data Protection (KMS, S3, ELB, EBS, RDS)
    - Incident Response (IAM, CloudFormation, CloudWatch Events)

## Third : Reliability
- Design Principles
    - Test recovery procedures
    - Automatically recover form failure
    - Scale horizontally to increase aggregate system availability
    - Stop gussing capacity
    - Manage change in automation
- Services
    - Foundations (IAM, VPC, Service Quotas, Trusted Advisor)
    - Change Management (Auto Scaling, CloudWatch, CloudTrail, Config)
    - Failure Management (Backups, CloudFormation, S3, Route 53)

---

## Fourth : Performance Efficiency
- Design Principles
    - Democratize advanced technologies
    - Go global in minutes
    - Use serverless architectures
    - Experiment more often
    - Mechanical sympathy
- Services
    - Selection (Auto Scaling, Lambda, EBS, S3, RDS)
    - Review (CloudFormation, AWS News Blog)
    - Monitoring (CloudWatch, Lambda, CloudTrail)
    - Tradeoffs (RDS, ElasticCache, SnowBall, CloudFront)

---

## Fifth : Cost Optimization
- Design Principles
    - Adopt a consumption mode
    - Measure overall efficiency
    - Stop spending money on data center operations
    - Analyze adn attribute expenditure
    - Use managed and app loevel services to reduce cost of ownership
- Services
    - Expenditure Awareness (Budgets, Cost & Usage Report, Cost Explorer, Reserved Instance Reporting)
    - Cost- Effective Resources (Spot, Reserved, S3 Glacier)
    - Matching supply and demand (Auto Scaling, Lambda)
    - Optimizing Over Time (Trusted Advisor, Cost & Usage Report, AWS News Blog)

---

## Well-Architected Tools (yes it's a service)
- Tool to review your architectures and adopt architectural best practice

## AWS Ecosystem
- AWS Blog
- AWS Forum
- Whitepaper & Guides
- Quick Starts
- AWS Solutions
- AWS Support
- AWS Marketplace
- AWS Training
- Professional Services & Partner Network
