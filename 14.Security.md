# Security

## Shared Responsibility Model
- AWS Responsibility - Security of the Cloud
    - Protecting AWS infra
    - Managed service like S3, DynamoDB, RDS, etc

- Customer Responsibility- Security in the Cloud
    - EC2 OS, Security patches & updates, firewall & network
    - IAM
    - Encrypting app data

- Shared controls
    - Patch management
    - Configuration management
    - Awareness & trainings

---

## Shield
- Shield Standard
    - No additional cost
    - SYN/UDP floods
    - Reflection attacks
    - Layer 3/4 attacks (IP[Network] & TCP/UDP[Transport])

- Shield Advanced- 24/7 premium DDosS protection
    - EC2, ELB, CloudFront, Global Accelerator & Route 53
    - $3000 per month per org
    - 24/7 response team

- CloudFront and Route 53
    - Global edge network
    - Combine with Shield

- Be ready to Auto-Scaling

---

## WAF- Web Applicaion Firewall
- Filter specific requests based on rules
- Layer 7 (HTTP[Application])
- Deploy on ALB, API Gateway, CloudFront

- Define WebACL
    - IP address, HTTP headers, HTTP body or URI
    - SQL injections & XSS
    - Geo match, Size constraints
    - Rate-based rules (count occurencesof events)- DDoS protection
    - More like CloudFlare proxy

---

## Penetration Testing on AWS
- Attack against AWS

- 8 services without prior approval
    - EC2, NAT Gateway, ELB
    - RDS
    - CloudFront
    - Aurora
    - API Gateways
    - Lambda & Lambda Edge functions
    - Lightsail resources
    - Elastic Beanstalk

- Prohibited
    - DNS zone walking
    - DoS, DDoS, Simulated DoS or DDoS
    - Port Flooding
    - Protocol Flooding
    - Request Flooding

---

## Encryption
- Two types
    - Data at rest - Stored or archived on AWS
    - Data in transit - encrypt on motion, transfer from one another on network

- KMS (Key Management Service)
    - AWS manages the encryption keys for us
    - Opt-in : EBS, S3, Redshift, RDS, EFS
    - Auto enabled : CloudTrail Logs, S3 Glacier, Storage Gateway

- CloudHSM
    - Encryption Physical Hardware
    - AWS manages the Hardware
    - Customer manage your own keys entirely

- Types of Customer Master Key: CMK
    - Customer Managed CMK:
        - Create, managed and used by customer
        - Possibility of rotation policy
        - Possibility of Bring-your-own-key

    - AWS managed CMK:
        - Created, manage and used on customer's behalf by AWS
        - Used on AWS services (S3, EBS, RedShift)
        
    - CloudHSM Keys(custom keystore):
        - Keys generated from your own CloudHSM hardware provided by AWS in AWS infra
        - Cryptographic ops are performed on CloudHSM

    - AWS owned CMK: 
        - Collection of CMKs, AWS owns and managed
        - AWS used them to protect resources in customer account info
        - Customer cannot view the keys
        - Cannot view event in KMS or any AWS dashboard

---

## Certificate Manager(ACM)
- Manage SSL/TLS certs
- Used to provide in-flight encryption for websites
    - ELB, CloudFront, API Gateway
- Support both public and private TLS certificates
- Free of charge for public TLS certificates(cannot export, use only in AWS)

---

## Secret Manager
- Integration with RDS, DB, API key
- Automate generation and rotation with Lambda

---

## Artifact (not really a service)
- Dashboard for AWS compliance documentation & AWS agreements
- Artifact Reports
- Artifact Agreements
- Can be used to support internal audit or compliance

---

## GuardDuty
- Use ML to detect anomaly, 3rd party data
- Inteligent Threat discovery
- One click to enable (30 days trial)

- Input data include:
    - CloudTrail Logs
    - VPC Flow Logs
    - DNS Logs

- Can setup CloudWatch Event rule to notified
- With CloudWatch Event, trigger AWS Lambda or SNS

---

## Inspector
- Automated Security for EC2
- OS vulnerabilities
- Analyze unintended network access
- Inspector must install on OS in EC2
- Output report
- Like antivirus scanner

---

## Config
- Auditing & reocrd compliance of your AWS resource
- Help record configurations
- Can store config data into S3 and analyze into Athena
- Can push SNS
- Per region service
- Can use with CloudTrail

---

## Macie
- Fully managed data security & data privacy
- Use ML & pattern matching to disocver sensitive data in AWS
- Scan S3 bucket for sensitive data

---

## Security Hub
- Central security tool
- Across serveral AWS acounts

- Hub for:
    - GuardDuty
    - Inspector
    - Macie
    - IAM Access Analyzer
    - System Manager
    - Forewall manager
    - Partner Network Solutions

- Must first enable AWS Config Service

---

## Detective
- Analyze, investigates, identifies the root cause of security issue
- Using ML & graph

---

## Abuse (Team)
- Report portal for AWS resource 
- Abusive or illegal purpose report
    - Spam
    - Port Scanning
    - DoS or DDoS
    - Intrusion attempts
    - Hosting objectionable or copyrighted content
    - Distributing malware

---

## Root user privilages
- Account owner
- Complete access
    
- Special actions only for root user
    - Change account settings
    - View tax invoices
    - Close AWS account
    - Restore IAM user permissions
    - Change or cancel AWS support plan
    - Register as a seller in Reserved Instance Marketplace
    - S3 bucket enable MFA
    - Edit S3 bucket policy that includes an invalid VPCID or VPC endpoint ID
    - Sign up for GovCloud